package com.techu.entregables.controlador;

import com.techu.entregables.modelo.ModeloProducto;
import com.techu.entregables.servicio.ServicioDatos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("${api.version}/productos")
public class ControladorProductos {

    @Autowired
    private ServicioDatos servicioDatos;

    @GetMapping
    public ResponseEntity obtenerProductos(){
        return ResponseEntity.ok(this.servicioDatos.obtenerProductos());
    }

    @PostMapping
    public ResponseEntity crearProducto(@RequestBody ModeloProducto producto){
        this.servicioDatos.agregarProducto(producto);
        return new ResponseEntity("Producto creado correctamente",HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity actualizarUnProducto(@PathVariable int id, @RequestBody ModeloProducto producto){
        ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if(p!=null){
            this.servicioDatos.actualizarProducto(id,producto);
            return new ResponseEntity("Producto actualizado correctamente.",HttpStatus.OK);
        }
        return new ResponseEntity("Producto no encontrado.",HttpStatus.NOT_FOUND);
    }

    @PatchMapping("/{id}")
    public ResponseEntity actualizarUnProductoPatch(@PathVariable int id, @RequestBody ModeloProducto producto){
        ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if(p!=null){
            this.servicioDatos.actualizarProducto(id,producto);
            return new ResponseEntity("Producto actualizado correctamente.",HttpStatus.OK);
        }
        return new ResponseEntity("Producto no encontrado.",HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{id}")
    public ResponseEntity obtenerUnProducto(@PathVariable int id){
        ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if(p!=null){
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity("Producto no encontrado.",HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity borrarUnProducto(@PathVariable int id){
        ModeloProducto p = this.servicioDatos.obtenerProductoPorId(id);
        if(p!=null){
            this.servicioDatos.borrarProducto(id);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity("Producto no encontrado.",HttpStatus.NOT_FOUND);
    }
}
