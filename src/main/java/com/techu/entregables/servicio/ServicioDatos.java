package com.techu.entregables.servicio;

import com.techu.entregables.modelo.ModeloProducto;
import com.techu.entregables.modelo.ModeloUsuario;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service //Le dice a Spring que maneje objetos de esta clase.
public class ServicioDatos {
    private final AtomicInteger secuenciaIdsProductos
            = new AtomicInteger(0);
    private final AtomicInteger secuenciaIdsUsuarios
            = new AtomicInteger(0);
    private final List<ModeloProducto> productos
            = new ArrayList<ModeloProducto>();
    //CREATE
    public ModeloProducto agregarProducto(ModeloProducto producto){
        producto.setId(this.secuenciaIdsProductos.incrementAndGet());
        this.productos.add(producto);
        return producto;
    }

    //CREATE
    public ModeloUsuario agregarUsuarioProducto(int idProducto, ModeloUsuario usuario){
        usuario.setId(this.secuenciaIdsUsuarios.incrementAndGet());
        this.obtenerProductoPorId(idProducto).getUsuarios().add(usuario);
        return usuario;
    }

    //READ
    public List<ModeloProducto> obtenerProductos(){
        return Collections.unmodifiableList(this.productos);
    }

    //READ
    public List<ModeloUsuario> obtenerUsuariosProductos(int idProducto){
        final ModeloProducto p = this.obtenerProductoPorId(idProducto);
        if(p == null)
            return null;
        return Collections.unmodifiableList(p.getUsuarios());
    }

    //READ
    public ModeloUsuario obtenerUsuarioProducto(int idProducto, int idUsuario){
        final ModeloProducto p = this.obtenerProductoPorId(idProducto);
        if(p == null)
            return null;
        for(ModeloUsuario u: p.getUsuarios()){
            if(u.getId() == idUsuario){
                return u; //OJO: se puede manipular desde afuera
            }
        }
        return null;
    }

    //READ
    public ModeloProducto obtenerProductoPorId(int id){
        for(ModeloProducto p: this.productos){
            if(p.getId() == id)
                return p; //OJO: se puede modificar desde afuera
        }
        return null;
    }
    //UPDATE
    public boolean actualizarProducto(int id, ModeloProducto producto){
        for(int i=0; i< this.productos.size(); ++i){
            if(this.productos.get(i).getId() == id) {
                producto.setId(id);
                this.productos.set(i, producto); //OJO: se puede modificar desde afuera
                return true;
            }
        }
        return false;
    }

    //DELETE
    public boolean borrarProducto(int id){
        for(int i=0;i<this.productos.size();++i){
            if(this.productos.get(i).getId() == id){
                this.productos.remove(i);
                return true;
            }
        }
        return false;
    }

    //DELETE
    public boolean borrarUsuarioProducto(int idProducto, int idUsuario){
        final ModeloProducto producto = this.obtenerProductoPorId(idProducto);
        if(producto == null)
            return false;
        List<ModeloUsuario> usuarios = producto.getUsuarios();
        for(int i = 0; i < usuarios.size(); ++i){
            if(usuarios.get(i).getId() == idUsuario){
                usuarios.remove(i);
                return true;
            }
        }
        return false;
    }
}
